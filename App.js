import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons'

import { Provider } from 'react-redux';
import store from './redux/store';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import DrawerContent from './App/component/drawerContent'
import Login from './App/login';
import SignUp from './App/signup';
import Home from './App/home';
import Profile from './App/profile';
import Details from './App/details';

const AuthStack = createStackNavigator();
const HomeDrawer = createDrawerNavigator();

const HomeDrawerScreen = () => (
    <HomeDrawer.Navigator initialRouteName= "Home" 
        drawerContent={props=><DrawerContent {...props}/>}
        drawerContentOptions={{
            activeTintColor: '#0C09AD',
            itemStyle: { marginVertical: 10, },
        }}
        drawerStyle={{
            backgroundColor: '#c6cbef',
        }}>
        <HomeDrawer.Screen options={{drawerIcon: () => <Icon
                size= {40}
                color={'#7B9DF5'}
                name='home'></Icon>}} name= "Home" component= {Home} />
        <HomeDrawer.Screen options={{drawerIcon: () => <Icon
                size= {40}
                color={'#7B9DF5'}
                name='details'></Icon>}} name= "Detail" component= {Details} />
        <HomeDrawer.Screen options={{drawerIcon: () => <Icon
                size= {40}
                color={'#7B9DF5'}
                name='account-circle'></Icon>}}
        name= "Profile" component= {Profile}/>
    </HomeDrawer.Navigator>
)

export default function App() {
  return (
    <Provider store= {store} >
        <NavigationContainer>
            <AuthStack.Navigator initialRouteName= "Login">
                <AuthStack.Screen name= "Login" component= {Login} options= {{ headerShown: false }}/>
                <AuthStack.Screen name= "Sign Up" component= {SignUp} options= {{ headerShown: false }}/>
                <AuthStack.Screen name= "Home" component= {HomeDrawerScreen} options= {{ headerShown: false }}/>
                <AuthStack.Screen name= "Detail" component= {Details} options={{ headerShown: false }}/>
            </AuthStack.Navigator>
        </NavigationContainer>
    </Provider>
  );
}

