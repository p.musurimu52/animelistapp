import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, FlatList, ScrollView} from 'react-native'

import { connect } from 'react-redux';

import Header from './component/header'
import {Foundation} from '@expo/vector-icons';
import Data from './component/animeListFav.json'
import AnimeList from './component/animeList'

class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myFavorit: this.props.userProfile.myFavorite
        }
    }
    
    render() {
        let p = this.props
        let data = p.userProfile
        console.log(data.myFavorite)
        return (
            <View style={styles.container}>
                <Header isLogin={p.isLogin} openDrawer= {() => p.navigation.toggleDrawer()}/>

                {/* <View style={styles.headerIcon}>
                    <Foundation name='list' size={35} style={styles.listIcon} />
                </View> */}
                <View style={styles.profile}>
                    <Image source={require('./images/profile.jpg')} style={styles.profilePicture}/>
                    <View style={styles.profileText}>
                        <Text style={styles.profileName}>{data.userName}</Text>
                        <Text style={styles.profileMember}>{data.status}</Text>
                    </View>
                </View>
                <View style={styles.favorite}>
                        <Text style={styles.favoriteHeader}>My Favorite List</Text>
                        {data.myFavorite.length > 0 ?
                            <View style={styles.favoriteContainer}>
                                <FlatList
                                    numColumns={2}
                                    data={this.state.myFavorite}
                                    renderItem={(result)=><AnimeList val={result.item} detailHandler= {() => this.detailHandler(result.item.mal_id)}/>}
                                    keyExtractor={(item, index)=>index}
                                />
                            </View>
                        : <View>
                            <Text>Your don't have favorite anime</Text>
                        </View>}
                    </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center'
    },
    headerIcon:{
        position: 'absolute',
        height:130,
        width: 36,
        top: 0,
        left: 12,
    },
    listIcon:{
        position: 'absolute',
        color: '#C4FAFA',
        top: 17,
        left: 7
    },
    profile:{
        height: 125,
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'center',
        padding: 20,
        borderBottomColor:'#0041BE',
        borderBottomWidth: 3
    },
    profilePicture:{
        height: 80,
        width: 80,
        borderRadius: 40,
        marginRight: 20,

    },
    profileText:{
        justifyContent: 'center',
        alignContent: 'center',
        height: 80
    },
    profileName:{
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 24,
        color: '#000E8C',
        textDecorationLine: 'underline',
        marginBottom: 5

    },
    profileMember:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#000E8C',
        marginBottom: 5
    },
    favorite:{
        marginHorizontal: 20,
        marginVertical: 10,

    },
    favoriteHeader:{
        color: '#0D1FBB',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 20,
    },
    favoriteContainer:{
        marginTop: 10,
        borderColor: '#000E8C',
        borderWidth: 3,
        borderRadius: 10,
        alignContent: 'center',
        padding: 20
    }

})

// >>>>Connector<<<<
function mapStateToProps (state) {
    return{
        isLogin: state.isLogin,
        userProfile: state.userProfile,
    }
}

export default connect(mapStateToProps, null)(profile)