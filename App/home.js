import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList, ScrollView, TouchableOpacity } from 'react-native'
import {Foundation} from '@expo/vector-icons'; 
import Axios from 'axios';
import { connect } from 'react-redux';

import Header from './component/header'
import AnimeList from './component/animeList'
// import HomeData from './component/animeListHome.json';
import { getData, changeTabCategory, doDetail } from '../redux/actions';

class home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabCategory: 'ongoing'
        };
    }

    componentDidMount() {
        Axios.get(`https://api.jikan.moe/v3/search/anime?q=Thriller&start_date=2017-01-01&end_date=2020-07-08&order_by=descending`)
            .then(res => {
                this.props.getData(res.data.results)
            }
        )
    }
        
    categoryHandler = (type) => {
        this.setState({tabCategory: type})
        this.props.changeTabCategory(type)
    }

    detailHandler = (id) => {
        let ndata = this.props.animeView.filter(d=>d.main_id == id)[0]
        this.props.doDetail(id)
        this.props.navigation.navigate('Detail', {refdata: ndata})
    }

    render() {
        let p = this.props
        let s = this.state
         //  If load data
        if (p.isError) {
            return (
                <View
                style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                <Text>Terjadi Error Saat Memuat Data</Text>
                </View>
            )
        }
        // if data ok
        else{
            return (
                <View style={styles.container}>
                    <Header isLogin={p.isLogin} openDrawer= {() => p.navigation.toggleDrawer()}/>
                    {/* <View style={styles.headerIcon}>
                        <Foundation name='list' size={35} style={styles.listIcon} />
                    </View> */}
                    <View style={styles.typeBar}>
                        <TouchableOpacity style= { s.tabCategory == 'ongoing' ? styles.typeUpdatedA : styles.typeUpdated} onPress={() => this.categoryHandler('ongoing')}>
                            <Text style={s.tabCategory == 'ongoing' ? styles.textUpdatedA : styles.textUpdated}>Updated</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={ s.tabCategory == 'completed' ?styles.typeCompletedA : styles.typeCompleted} onPress={() => this.categoryHandler('completed')}>
                            <Text style={s.tabCategory == 'completed' ? styles.textCompletedA : styles.textCompleted}>Completed</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.animeListContainer}>
                        <FlatList
                            numColumns={2}
                            data={p.animeView}
                            renderItem={(list)=><AnimeList val={list.item} detailHandler= {() => this.detailHandler(list.item.mal_id)} />}
                            keyExtractor={(item, index) => index.toString()}
                            ItemSeparatorComponent={ ()=><View style={{height:2, backgroundColor:'rgba(123, 157, 245, 0.63)', margin: 15 }} /> }
                        />
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center'
    },
    headerIcon:{
        position: 'absolute',
        height:130,
        width: 36,
        top: 0,
        left: 12,
    },
    listIcon:{
        position: 'absolute',
        color: '#C4FAFA',
        top: 17,
        left: 7
    },
    typeBar:{
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 17
    },

    // category Active    
    typeUpdatedA:{
        width: 124,
        height: 28,
        backgroundColor: '#000E8C',
        borderTopStartRadius: 5,
        borderBottomStartRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textUpdatedA:{
        fontFamily: 'Roboto',
        fontWeight: '500',
        color: '#FEFFFF',
        fontSize: 14
    },
    textCompletedA:{
        fontFamily: 'Roboto',
        fontWeight: '500',
        color: '#FEFFFF',
        fontSize: 14
    },
    typeCompletedA:{
        width: 124,
        height: 28,
        backgroundColor: '#000E8C',
        borderTopEndRadius: 5,
        borderBottomEndRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    // category non Active
    typeUpdated:{
        width: 124,
        height: 28,
        backgroundColor:'#FEFFFF',
        borderTopStartRadius: 5,
        borderBottomStartRadius: 5,
        borderWidth: 2,
        borderColor:'#000E8C',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textUpdated:{
        fontFamily: 'Roboto',
        fontWeight: '500',
        color: '#000E8C',
        fontSize: 14
    },
    textCompleted:{
        fontFamily: 'Roboto',
        fontWeight: '500',
        color: '#000E8C',
        fontSize: 14
    },
    typeCompleted:{
        width: 124,
        height: 28,
        backgroundColor:'#FEFFFF',
        borderTopEndRadius: 5,
        borderBottomEndRadius: 5,
        borderWidth: 2,
        borderColor:'#000E8C',
        alignItems: 'center',
        color: '#FEFFFF',
    },
    animeListContainer:{
        marginTop: 20,
        padding: 10,
        alignSelf: 'center',
        flexWrap: 'wrap',
        // width: 150,
        // justifyContent: 'space-around'
    }
})

// >>>>>connector<<<<<
function mapStateToProps (state) {
    return {
        isLogin: state.isLogin,
        animeList: state.animeList,
        animeView: state.animeView,
        tabCategory: state.tabCategory,
    }
}

function mapDispatchToProps (dispatch) {
    return {
        getData: (refdata) => dispatch(getData(refdata)),
        changeTabCategory: (category) => dispatch(changeTabCategory(category)),
        doDetail: (id) => dispatch(doDetail(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(home)
