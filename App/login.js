import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'

import { connect } from 'react-redux';

import { doLogin } from '../redux/actions';
import Header from './component/header'

class login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            textErr:'',
            userError: false,
            passError: false,
        }
    }

    loginHandler() {
        let userIndex = this.props.users.findIndex(d=>d.userName == this.state.userName)
        if(this.state.userName == "" && this.state.password == ""){
            this.setState({textErr:"Please insert ", userError: true, passError: true})
            setTimeout(()=>{
                this.setState({textErr:"", userError: false, passError: false})
            },3000)
        }else if(this.state.userName == ""){
            this.setState({textErr:"Please insert ", userError: true, })
            setTimeout(()=>{
                this.setState({textErr:"", userError: false})
            },3000)
        }else if(this.state.password == ""){
            this.setState({textErr:"Please insert ",  passError: true})
            setTimeout(()=>{
                this.setState({textErr:"", passError: false})
            },3000)
        } else if(userIndex == -1){
            this.setState({textErr:" user not found!! Sign up, Please", userError: true})
            setTimeout(()=>{
                this.setState({textErr:"", userError: false})
            },3000)
        }else{
            let userData = this.props.users[userIndex] 
            if(this.state.password == userData.password){
                this.props.doLogin(this.state.userName, this.state.password)
                this.props.navigation.navigate( "Home" )
            } else{
                this.setState({textErr:" Salah", passError: true})
                setTimeout(()=>{
                    this.setState({textErr:"", passError: false,})
                },3000)
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Header isLogin={this.props.isLogin} />
                <View style={styles.body}>
                    <Text style={styles.title}>Member Log In</Text>
                    <View>
                        <Text style={styles.loginText}>Username/Email
                            <Text style={this.state.userError ? styles.errorText : styles.hiddenErrorText}> {this.state.textErr}</Text>
                        </Text>
                        <TextInput style={styles.loginInput}
                            value= {this.state.userName}
                            placeholder='insert your email'
                            onChangeText={userName => this.setState({ userName })} />
                        <Text style={styles.loginText}>Password
                            <Text style={this.state.passError ? styles.errorText : styles.hiddenErrorText}> {this.state.textErr}</Text>
                        </Text>
                        <TextInput style={styles.loginInput}                            
                            value= {this.state.password}
                            placeholder='insert your password'
                            onChangeText={password => this.setState({ password })}
                            secureTextEntry={true}/>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={() => this.loginHandler()}>
                        <Text style={styles.buttonText}>Log In</Text>
                    </TouchableOpacity>
                    <Text style={styles.text}>Or</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.push("Sign Up")}>
                        <Text style={styles.signUp}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center'
    },
    body:{
        alignSelf: 'center',
        alignItems: 'center'
    },
    title:{
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#0C09AD',
        marginVertical: 50,
    },
    loginText:{
        color: '#0C09AD',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 14,
    },
    loginInput:{
        height: 40,
        width: 260,
        borderWidth: 2,
        borderColor : '#000AEE',
        borderRadius: 5,
        marginBottom: 20,
    },
    button:{
        height: 35,
        width: 70,
        borderRadius: 5,
        backgroundColor: '#0754C7',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText:{
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#96E6FF',
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: 18,
        color: '#0C09AD',
        marginVertical: 12  
    },
    signUp:{
        fontFamily: 'Roboto',
        fontSize: 18,
        color: '#0C09AD',
        fontWeight: 'bold'
    },

    // Error Login
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },

})

// >>>>Connector<<<<
function mapStateToProps (state) {
    return{
        isLogin: state.isLogin,
        users: state.users,
    }
}

function mapDispatchToProps ( dispatch ) {
    return {
        doLogin: (userName, password)=>dispatch(doLogin(userName, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(login)

