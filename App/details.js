import React, { Component } from 'react'
import {Text, View, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native'
import {Foundation, MaterialIcons} from '@expo/vector-icons'; 

import Header from './component/header';
import { connect } from 'react-redux';
import { addMyFavorite } from '../redux/actions';

const DEVICE = Dimensions.get('window')

class Details extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: this.props.animeDetail
        }
    }
    render() {
        let data = this.props.animeDetail
        return (
            <View style={styles.container}>
                <Header isLogin={this.props.isLogin} openDrawer= {() => this.props.navigation.toggleDrawer()}/>
                {/* <View style={styles.headerIcon}>
                    <Foundation name='list' size={35} style={styles.listIcon} />
                </View> */}
                {
                    data.title == undefined ?
                    <View style={{backgroundColor: '#7B9DF5', height: DEVICE.height*0.3}}>
                        <Text style= {{fontSize:25, padding:20, margin:25, textAlign:"center" }}>Please select one title for this Page</Text>
                        <TouchableOpacity onPress= {()=>this.props.navigation.navigate('Home')} style={styles.rateButton}>
                            <Text style={styles.buttonText}>Back To Home</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View>
                        <View style={styles.detailsBackground}>
                            <View style= {styles.detailsSection}>
                                <Image source={{uri:`${data.image_url}` }} style={styles.detailsCover}/>
                            </View>
                            <View style= {styles.detailsList}>
                                <View style={styles.detailInfo}>
                                    <View style={{ width: DEVICE.width*0.4}}>
                                        <Text style={styles.detailsTitle}>{data.title}</Text>
                                        <View style={styles.inline}>
                                            <Text style={styles.detailsType}>Status: </Text>
                                            <Text style={styles.detailsTypeValue}>{data.end_date == null ? "ONGOING" : "COMPLETED"}</Text>
                                        </View>
                                        <View style={styles.inline}>
                                            <Text style={styles.detailsType}>Score: </Text>
                                            <Text style={styles.detailsTypeValue}>{data.score}</Text>
                                        </View>
                                        <View style={styles.inline}>
                                            <Text style={styles.detailsType}>Episodes: </Text>
                                            <Text style={styles.detailsTypeValue}>{data.episodes}</Text>
                                        </View>
                                        <View style={styles.inline}>
                                            <Text style={styles.detailsType}>Airing: </Text>
                                            <Text style={styles.detailsTypeValue}>{data.start_date != null ? data.start_date.slice(0, 10) : ""}</Text>
                                        </View>
                                    </View>
                                    <View style={{width: DEVICE.width*0.1, padding:5}}>
                                        <TouchableOpacity onPress= {() => this.props.addMyFavorite(data, this.props.userProfile)}>
                                            <MaterialIcons name='playlist-add' size={30} style={styles.addButton} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{alignSelf:'center'}}>
                                    <TouchableOpacity style={styles.rateButton}>
                                        <Foundation name='star' size={15} style={styles.rateStar}/>
                                        <Text style={styles.buttonText}>Rate It!</Text>
                                    </TouchableOpacity>
                                </View>
                                
                            </View>
                        </View>
                        <View style={styles.synopsisContainer}>
                            <Text style={styles.synopsisTitle}>Synopsis:</Text>
                            <Text style={styles.synopsisFill}>{data.synopsis}</Text>
                        </View>
                    </View>
                }
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    headerIcon:{
        position: 'absolute',
        height:130,
        width: 36,
        top: 0,
        left: 12,
    },
    listIcon:{
        position: 'absolute',
        color: '#C4FAFA',
        top: 17,
        left: 7
    },
    addButton:{
        // position: 'absolute',
        color: '#C4FAFA',
        // top: 25,
        // right: 10
    },
    detailsBackground:{
        backgroundColor: '#7B9DF5', 
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center' 
    },
    detailsSection:{
        height: 200, 
        marginVertical:10, 
        marginLeft:20,
        marginHorizontal: 10,
        width: DEVICE.width*0.4,  
        borderRadius:5
    },
    detailsCover:{
        height: 200,
        width: DEVICE.width*0.4,
        borderRadius:10,
        alignSelf: "flex-end",
    },
    detailsList:{
        width: DEVICE.width*0.55,  
        height: 200, 
        marginRight:20
    },
    detailInfo:{
        flexDirection:"row", 
        justifyContent:'center', 
        alignItems:'center'
    },
    detailIcon:{

    },
    detailsTitle:{
        // width: 180,
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        color: '#FEFFFF',
        borderBottomWidth: 2,
        borderBottomColor: 'rgba(254, 255, 255, 0.63)'
    },
    detailsType:{
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#FEFFFF'
    },
    detailsTypeValue:{
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#FEFFFF'
    },
    inline:{
        flexDirection: 'row',
        marginTop: 5
    },
    rateButton:{
        marginVertical: 15,
        paddingVertical:5,
        paddingHorizontal:15,
        flexDirection: 'row',
        // width: 75,
        // height: 20,
        backgroundColor: '#000E8C',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    buttonText:{
        fontSize:12,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        color: '#FEFFFF'
    },
    rateStar:{
        color: '#FEFFFF',
        marginRight: 5
    },
    synopsisContainer:{
        marginTop: 10,
        flexWrap: 'wrap',
        marginHorizontal: 16
    },
    synopsisTitle:{
        color: '#000E8C',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        borderBottomWidth: 2,
        borderBottomColor: 'rgba(123, 157, 245, 0.63)',
        paddingBottom: 7
    },
    synopsisFill:{
        marginTop: 10,
        marginHorizontal: 14,
        color: '#000E8C',
        fontFamily: 'Roboto',
        fontSize: 14,
        width: 300
    }
    
})

// >>>>Connector<<<<
const mapStateToProps = (state) => {
    return {
        isLogin: state.isLogin,
        animeDetail: state.animeDetail,
        userProfile: state.userProfile,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addMyFavorite: (refdata, refprofile) => dispatch(addMyFavorite(refdata, refprofile))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)