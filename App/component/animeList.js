import React,{Component} from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'

export default class AnimeList extends Component{
    render(){
        return(
            <View style={styles.listContainer}>
                <Image source={{uri:this.props.val.image_url}} style={styles.cover}/>                
                <TouchableOpacity onPress={() => this.props.detailHandler()}>
                    <Text style={styles.title}>{this.props.val.title}</Text>
                </TouchableOpacity>
                <Text style={styles.textList}>Type: {this.props.val.type}</Text>
                <Text style={styles.textList}>Score: {this.props.val.score} </Text>
                <Text style={styles.textList}>Rated: {this.props.val.rated} </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    listContainer:{
        margin: 15,
        // marginRight: 15,
        // marginBottom: 15,
        // borderBottomWidth: 2,
        // borderBottomColor: 'rgba(123, 157, 245, 0.63)',
        // paddingBottom: 8,
        // alignSelf: 'center',
        width: 150,
        justifyContent: 'space-around'
    },
    cover:{
        height: 120,
        width: 80,
        alignSelf: 'flex-start',
        marginBottom: 10,
        borderRadius:5,
    },
    title:{
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 16,
        color: '#0D1FBB',
        marginBottom: 5  ,
        flexWrap: 'wrap',

    },
    textList:{
        fontSize: 14,
        fontFamily: 'Roboto',
        color: '#0D1FBB',
        marginBottom: 5
    },
})

