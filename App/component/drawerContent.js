import React,{Component} from 'react'
import { View, StyleSheet } from 'react-native'
import {DrawerContentScrollView, DrawerItem, DrawerItemList} from '@react-navigation/drawer'
import{
        Avatar,
        Paragraph,
        Drawer,
        Text,
        TouchableRipple,
} from 'react-native-paper'



let p = this.props
let data = p.userProfile
const DrawerContent = props => {
    return(
        <View>
            <View style={styles.profile}>
                <Avatar.Image source={require('../images/profile.jpg')} style={styles.profilePicture} size={80}/>
                <View style={styles.profileText}>
                    <Text style={styles.profileName}>{data.userName}</Text>                   
                        
                    {/* <Text style={styles.profileView}>View profile</Text> */}
                </View>
            </View>
            <DrawerItemList {...props}>
                {/* <DrawerItem
                    icon={({color, size})=>(
                    <Icon name='home' color={color} size={size}/>
                    )}
                    label='Home'
                    onPress={()=>{}}
                />
                <DrawerItem
                    icon={({color, size})=>(
                    <Icon name='details' color={color} size={size}/>
                    )}
                    label='Detail'
                    onPress={()=>{}}
                /> */}
            </DrawerItemList>
        </View>
    )
}

const styles = StyleSheet.create({
    profile:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        padding: 10,
        borderBottomColor:'#0041BE',
        borderBottomWidth: 3,
        backgroundColor: '#000E8C'
    },
    profilePicture:{
        marginRight: 20,

    },
    profileText:{
        justifyContent: 'center',
        alignContent: 'center',
        height: 80
    },
    profileName:{
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 24,
        color: '#C4FAFA',
        marginBottom: 5

    },
    profileView:{
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#C4FAFA',
        
    },
})

export default DrawerContent;
