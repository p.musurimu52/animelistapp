import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native'
import {Foundation} from '@expo/vector-icons'; 

const DEVICE = Dimensions.get('window')

export default function header(props) {
    // return (
                // <View style={styles.header}>
                //     <Image source={require('../images/logo.png')} style={styles.logo}/>
                // </View>
    if(props.isLogin){
        return <View style={styles.header}>
                <View style= {styles.headerLeft}>
                    <TouchableOpacity onPress= {() => props.openDrawer()}>
                        <Foundation name='list' size={35} style={styles.headerIcon} />
                    </TouchableOpacity>
                </View>
                <View  style= {styles.headerRight}>
                    <Image source={require('../images/logo.png')} style={styles.logo}/>
                </View>
            </View>
    }else{
        return <View style={styles.header}>
                <View>
                    <Image source={require('../images/logo.png')} style={styles.logo}/>
                </View>
            </View>
    }
    // )
}

const styles = StyleSheet.create({
    header:{
        width: DEVICE.width,
        flexDirection: 'row',
        alignContent: 'center',
        alignItems:'center',
        height: 130,
        backgroundColor: '#000E8C',
        justifyContent: 'center',
        borderBottomWidth: 4,
        borderBottomColor: '#136AEC'
    },
    logo:{
        height: 45,
        width: 230,
        alignSelf: 'center'
    },
    headerLeft: {
        marginHorizontal: 5,
        justifyContent: 'flex-start',
        width: DEVICE.width*0.15,        
    },
    headerIcon: {
        alignSelf: 'flex-start',
        color: 'white',
        margin: 15
    },  
    headerRight: {
        width: DEVICE.width*0.85,
    },  
    headerText: {
        color:'white',
        fontWeight: 'bold',
        fontSize: 18,
        alignSelf:'center'
    },
})

