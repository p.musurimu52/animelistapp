import 'react-native-gesture-handler';
import React, { Component } from 'react'
import {StyleSheet} from 'react-native'
import {NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import {createDrawerNavigator} from '@react-navigation/drawer'

import Home from '../App/home'
import Details from '../App/details'
import Profile from '../App/profile'
import Login from '../App/login'
import SignUp from '../App/signup'

const HomeStack = createStackNavigator();
// const DetailsStack = createStackNavigator();
const ProfileStack = createStackNavigator();
// const LoginStack = CreateStackNavigator();
// const SignUpStack = CreateStackNavigator();
const Drawer = createDrawerNavigator();

const HomeStackScreen =()=>{
    <HomeStack.Navigator>
        <HomeStack.Screen name='Home' component={Home} />
    </HomeStack.Navigator>
}

const ProfileStackScreen =()=>{
    <ProfileStack.Navigator>
        <ProfileStack.Screen name='Profile' component={Profile} />
    </ProfileStack.Navigator>
}

export default class NavigationScreen extends Component{
    render(){
        return(
            <NavigationContainer style={styles.container}>
                <Drawer.Navigator>
                    <Drawer.Screen name="Profile" component={ProfileStackScreen} />
                    <Drawer.Screen name="Home" component={HomeStackScreen}   />
                </Drawer.Navigator>
            </NavigationContainer>
        )
    }
}
const styles = StyleSheet.create({

});
