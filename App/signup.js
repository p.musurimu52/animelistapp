import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'

import { connect } from 'react-redux';

import { doRegister } from '../redux/actions';
import Header from './component/header'

class signup extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            upassword: '',
            textErr: '',
            userError: false,
            passError: false,
            upassError: false,
        }
    }

    registerHandler=()=>{
        let s = this.state
        if(s.userName == ""){
            this.setState({textErr:" Please insert", userError: true})
            setTimeout(()=>{this.setState({textErr:"", userError: false,})},3000)
        }
        if(s.password == ""){
            this.setState({textErr:" Please insert", passError: true})
            setTimeout(()=>{this.setState({textErr:"", passError: false,})},3000)
        }
        if(s.upassword == ""){
            this.setState({textErr:" Please insert", upassError: true})
            setTimeout(()=>{this.setState({textErr:"", upassError: false,})},3000)
        }
        if(s.upassword != s.password){
            this.setState({textErr:" Password not match", upassError: true})
            setTimeout(()=>{this.setState({textErr:"", upassError: false,})},3000)
        }
        if(s.upassword != "" && (s.upassword == s.password)){
            this.props.doRegister(s.userName, s.password)
            this.props.navigation.navigate( "Login" )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <View style={styles.body}>
                    <Text style={styles.title}>Sign Up</Text>
                    <View>
                        <Text style={styles.loginText}>Username: 
                            <Text style={this.state.userError ? styles.errorText : styles.hiddenErrorText}> {this.state.textErr}</Text>
                        </Text>
                        <TextInput style={styles.loginInput}
                            value= {this.state.userName}
                            onChangeText={text => this.setState({userName: text})}
                            />
                        {/* <Text style={styles.loginText}>Email:</Text>
                        <TextInput style={styles.loginInput}/> */}
                        <Text style={styles.loginText}>Password:
                            <Text style={this.state.passError ? styles.errorText : styles.hiddenErrorText}>{this.state.textErr}</Text>
                        </Text>
                        <TextInput style={styles.loginInput} 
                            value= {this.state.password}
                            onChangeText={text => this.setState({password: text})}
                            secureTextEntry={true}
                        />
                        <Text style={styles.loginText}>Re-type Password:
                            <Text style={this.state.upassError ? styles.errorText : styles.hiddenErrorText}>{this.state.textErr}</Text>
                        </Text>
                        <TextInput style={styles.loginInput} 
                            value= {this.state.upassword}
                            onChangeText={text => this.setState({upassword: text})}
                            secureTextEntry={true}/>

                    </View>
                        <TouchableOpacity style={styles.button} onPress= {() => this.registerHandler()}>
                            <Text style={styles.buttonText}>Sign Up</Text>
                        </TouchableOpacity>
                        <Text style={styles.text}>Or</Text>
                        <TouchableOpacity  onPress={() => this.props.navigation.push("Login")}>
                            <Text style={styles.signUp}>Log In</Text>
                        </TouchableOpacity>
                </View>  
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center'
    },
    body:{
        alignSelf: 'center',
        alignItems: 'center'
    },
    title:{
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#0C09AD',
        marginVertical: 50,
    },
    loginText:{
        color: '#0C09AD',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 14,
    },
    loginInput:{
        height: 40,
        width: 260,
        borderWidth: 2,
        borderColor : '#000AEE',
        borderRadius: 5,
        marginBottom: 20
    },
    button:{
        height: 35,
        width: 70,
        borderRadius: 5,
        backgroundColor: '#0754C7',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText:{
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#96E6FF',
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: 18,
        color: '#0C09AD',
        marginVertical: 12  
    },
    signUp:{
        fontFamily: 'Roboto',
        fontSize: 18,
        color: '#0C09AD',
        fontWeight: 'bold'
    },

      // Error Login
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },

})

// >>>>Connector<<<<
const mapStateToProps = (state) => {
    return{
        isLogin: state.isLogin,
        users: state.users
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        doRegister:  (userName, password) => dispatch(doRegister(userName, password))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(signup)
