import { DO_LOGIN, DO_REGISTER, GET_DATA, CHANGE_TAB_CATEGORY, DO_DETAILS, ADD_MY_FAVORITE } from './type'

// initial State
const initialState = {
    users: [
        {
            userName: "a",
            password: '1',
            status: 'Reguler member',
            myFavorite: []
        }
    ],
    // data
    animeList: [],
    animeView: [],
    animeDetail: {},
    isLoading: false,
    tabCategory: 'ongoing',

    userProfile: {},
    isLogin: false,
}

export default (state = initialState, action) => {
    let ndata = action.data
    switch (action.type) {
        case DO_LOGIN: 
            return {
                ...state, 
                userProfile: state.users.filter(d=> d.userName == ndata.userName)[0],
                isLogin: true,
            }
        case DO_REGISTER: 
            return {
                ...state, 
                users: state.users.concat({
                    userName: ndata.userName,
                    password: ndata.password,
                    status: 'Reguler member',
                    myFavorite: []
                }),
            }
        case GET_DATA: 
            return {
                ...state,
                animeList: ndata,
                animeView: state.tabCategory == 'ongoing' ? ndata.filter(d=> d.end_date == null) : ndata.filter(d=> d.end_date != null)
            }
        case CHANGE_TAB_CATEGORY:
            return {
                ...state,
                data: ndata,
                animeView: ndata == 'ongoing' ? state.animeList.filter(d=> d.end_date == null) : state.animeList.filter(d=> d.end_date != null)
            }
        case DO_DETAILS:
            return {
                ...state,
                animeDetail: state.animeView.filter(d => d.mal_id == ndata)[0]
            }
        case ADD_MY_FAVORITE:            
            return {
                ...state,
                userProfile: ndata
            }
        default: 
            return state
    }
}