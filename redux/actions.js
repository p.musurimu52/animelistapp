import { DO_LOGIN, DO_REGISTER, GET_DATA, CHANGE_TAB_CATEGORY, DO_DETAILS, ADD_MY_FAVORITE } from './type';

export const doLogin =  (userName, password) => {
    return {
    type: DO_LOGIN,
    data: {userName, password}
    }
}

export const doRegister = (userName, password) => ({
    type: DO_REGISTER,
    data: {userName, password}
})

export const getData = (refdata) => ({
    type: GET_DATA,
    data: refdata
})

export const changeTabCategory = (category) => ({
    type: CHANGE_TAB_CATEGORY,
    data: category
})

export const doDetail = (id) => ({
    type: DO_DETAILS,
    data: id
})

export const addMyFavorite = (refdata, refprofile) => {
    let nfav = [...refprofile.myFavorite, refdata]
    
    refprofile.myFavorite = nfav
    return {
    type: ADD_MY_FAVORITE,
    data: refprofile
}}