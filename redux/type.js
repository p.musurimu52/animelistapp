export const DO_LOGIN = "DO_LOGIN"
export const DO_REGISTER = "DO_REGISTER"

export const GET_DATA = "GET_DATA"
export const CHANGE_TAB_CATEGORY = "CHANGE_TAB_CATEGORY"
export const DO_DETAILS = "DO_DETAILS"

export const ADD_MY_FAVORITE = "ADD_MY_FAVORITE"